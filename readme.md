# How to run for Strava club activities

Prerequisites:
- clone or fork this repository
- Application in Strava via settings
- [Python3](https://www.python.org/downloads/)
- Terminal or editor (vscode recommended)

## Setup
- Clone repo
- Create virtual environment: `python3 -m venv venv`
- Activate virutal environment: `source venv/bin/activate`

## Authentication
Updates `~/.bash_profile` with the following variables. Run `source ~/.bash_profile` in terminal after updates.

Add this block to your bash profile. Replace values between quotes. Find the values in Strava application settings
``` text
# STRAVA
export st_client_id='client id'
export st_client_secret='secret'
export st_access_token='access token'
export st_refresh_token='refresh token'
export club_id='club id'
```

These variables are then passed into python script using `import os` package
``` python
client_id = os.environ['st_client_id']
client_secret = os.environ['st_client_secret']
refresh_token = os.environ['st_refresh_token']
club_id = os.environ['club_id']
```

## Run
In VSCode, open py file and select play button on top right. Python can be installed here.

In terminal, activate python3 and supply py path to run.

## Output
Using the API this scripts opens JSON, reads data, writes to CSV. A file is then created in your root directory.

## Enhancements Needed

**Pagination** - Sometime between August and November 2021, Strava must have made some changes to API to limit records on pagination. Previously we were able to pull over 200 records on page: 1. Now there's a limit of 30 per page. This means we have to manually update page parameter and re-run script to get all desired activities. `param = {'per page': 200, 'page': 1}`

- Action: Update script to loop through each page to return all records.

**Dates** - Dates are not included with activities.

- Action: explore other API calls that include date
- Action: append date on pull - requires daily, scheduled runs.