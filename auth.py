# https://www.youtube.com/watch?v=2FPNb1XECGs
# https://medium.com/swlh/using-python-to-connect-to-stravas-api-and-analyse-your-activities-dummies-guide-5f49727aac86
import requests
# import os

activities_url = 'https://www.strava.com/api/v3/athlete/activities'

# Authentication
# client_id = os.environ['st_client_id']
# client_secret = os.environ['st_client_secret']
# refresh_token = os.environ['st_refresh_token']

temp_token = ''

# Activities
header = {'Authorization': temp_token}
print(header)
param = {'per_page': 200, 'page': 1}
print(param)

activities = requests.get(activities_url, headers=header, params=param).json()

print(activities)
