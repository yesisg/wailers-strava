import requests
import json
import pandas
import csv
import os
import urllib3
# disable warnings
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

auth_url = 'https://www.strava.com/oauth/token'

# Authentication
client_id = os.environ['st_client_id']
client_secret = os.environ['st_client_secret']
refresh_token = os.environ['st_refresh_token']
club_id = os.environ['club_id']

# urls
auth_url = 'https://www.strava.com/api/v3/oauth/token'
activities_url = "https://www.strava.com/api/v3/activities"
club_url = f'https://www.strava.com/api/v3/clubs/{club_id}/activities'

payload = {
    'client_id': client_id,
    'client_secret': client_secret,
    'refresh_token': refresh_token,
    # 'code': temp_code,
    # 'grant_type': 'authorization_code',
    'grant_type': 'refresh_token',
    # 'scope': 'read,activity:read_all',
    'f': 'json'
}

print('Requesting Token...\n')

response = requests.post(auth_url, data=payload, verify=False)

access_token = response.json()['access_token']
print('Access token = {}...\n'.format(access_token))

header = {'Authorization': 'Bearer ' + access_token}
param = {'per page': 200, 'page': 1}
# my_dataset = requests.get(club_url, headers=header, params=param).json()
#
# print(my_dataset)

# Club activities
club_r = requests.get(club_url, headers=header, params=param).json()

filename = 'club_wailers_data.txt'

# Writing JSON to a File
with open(filename, 'w') as outfile:
    filename = json.dump(club_r, outfile)

# Opening JSON file and loading the data
# into the variable data
with open('club_wailers_data.txt') as json_file:
    data = json.load(json_file)

# now we will open a file for writing
data_file = open('club_wailers.csv', 'w')

# create the csv writer object
csv_writer = csv.writer(data_file)

# Counter variable used for writing
# headers to the CSV file
count = 0

for ath in data:
    athlete_data = ath['athlete']
    athlete_name = athlete_data['firstname']
    # print(ath.keys())

    if count == 0:

        header = ath.keys()
        csv_writer.writerow(header)
        count += 1

    csv_writer.writerow(ath.values())

data_file.close()
print('Data written to CSV. Job complete.')
